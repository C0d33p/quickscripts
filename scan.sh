#!/bin/bash

# The script is supposed to check the folder permissions, and control if the users are not duplicated in both:
# ACL and group assigned to the folder.


# BEGIN MAIN SCRIPT
# =================

while getopts ":p:a" opt; do
    case "${opt}" in
	-p| --project) PN="$OPTARG"    ;; # PN = ProjectName
	-a| --alternative) AN="$OPTARG"    ;; # AN = AlternativeName
	'?'| -h| --help| *) echo "$0: invalid option -$OPTARG" >&2
	      echo "Usage: $0 [ -p project] [ -a alternative path]">&2
	      exit 1 ;;
    esac
done
shift $(($OPTIND - 1))

# Input check
if [ $# -eq 0 ] ; then
    echo "Usage: $"
fi

checkTheDest {
    find /path/to/projects -name $PN -type d -print 
        while read dirname
	    do
	        getfacl $dirname
	    done
	return 0
}

if [ -z "${p}" ] || [ -z "${a}" ]; then
   echo checkTheDest();
fi

# For testing
echo "p = ${p}"
echo "a = ${a}"
